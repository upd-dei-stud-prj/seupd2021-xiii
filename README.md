# Goemon Ishikawa Team: Touch� Task 1

_Argument Retrieval for Controversial Question_

## Introduction

This project provides a solution for the Touch� Task 1: Argument Retrieval for Controversial Questions. The goal is to
support users who search for arguments used in conversations. Given a question on a controversial topic, the task is to retrieve arguments from a focused crawl of online portals.

## Documentation

The report for HW1 can be found here [HW1-Goemon.pdf](HW1-Goemon.pdf). 

The report for HW2 can be found here [HW2-Goemon.pdf](HW2-Goemon.pdf).

The presentation slides can be found here [Goemon-slides.pdf](Goemon-slides.pdf)

## Partecipants

- Leonardo Carlassare
- Matteo Carnelos
- Laura Menotti
- Thomas Porro
- Gianmarco Prando
